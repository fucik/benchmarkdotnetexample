﻿
 using BenchmarkDotNet.Running;
using System.ComponentModel;
using System.Text;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Collections.Generic;

 // var a =new Demo();
// a.Test();
// a.Test4();
namespace CSharpTutorials
{
    class Program
    {
        static void Main(string[] args)
        {
           var results = BenchmarkRunner.Run<Demo>();
        }
    }




[SimpleJob(RuntimeMoniker.Net461)]
[SimpleJob(RuntimeMoniker.Net481)]
// [SimpleJob(RuntimeMoniker.NetCoreApp31,baseline:true)]
[SimpleJob(RuntimeMoniker.Net50)]
[SimpleJob(RuntimeMoniker.Net60)]
 [SimpleJob(RuntimeMoniker.Net70)]
[MemoryDiagnoser]
//[RPlotExporter]
public class Demo
{
    private BaseShape[] _shapes;
    private MyShape[] _myShapes;
    // public Demo()
    // {
    //     _shapes.Add(new Circle(3));
    //     _shapes.Add(new Rectangle(7,8));
    //     _shapes.Add(new Square(3));
    //     _shapes.Add(new Triangle(9,3));
    // }
    [GlobalSetup]
    public void Setup()
    {
        var shapes =new List<BaseShape>();
        shapes.Add(new Circle(3));
        shapes.Add(new Rectangle(7,8));
        shapes.Add(new Square(3));
        shapes.Add(new Triangle(9,3));
        _shapes = shapes.ToArray();
        
        List<MyShape> mshapes =new List<MyShape>();
        mshapes.Add(new MyShape(){w = 3,h = 9,shapeEnum = ShapeEnum.Circle});
        mshapes.Add(new MyShape(){w = 3, h = 9,shapeEnum = ShapeEnum.Rectangle});
        mshapes.Add(new MyShape(){w = 3, h = 9,shapeEnum = ShapeEnum.Square});
        mshapes.Add(new MyShape(){w = 3, h = 9,shapeEnum = ShapeEnum.Triangle});
        _myShapes = mshapes.ToArray();
    }
    
    [Benchmark(Baseline = true)]
    //[Benchmark]
    public float Test()
    {
        // for (int i = 0; i < 1000; i++)
        // {
       return     TotalArea(100,_shapes);
      //  }

       // return 0;
    }
    
    [Benchmark]
    public float Test4()
    {
        // for (int i = 0; i < 1000; i++)
        // {
      return      TotalArea4(100,_shapes);
        // }
        //
        // return 0;
    }
    
    [Benchmark]
    public float TestSwitch4()
    {
        // for (int i = 0; i < 1000; i++)
        // {
        return      TotalAreaSwitch4(100,_myShapes);
        // }
        //
        // return 0;
    }

    private float TotalArea(int count, BaseShape[] shapes)
    {
        float accum = 0;

        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < 4; j++)
            {
  accum += shapes[j].Area();
            }

        }

        return accum;
    }
    
    private float TotalArea4(int count, BaseShape[] shapes)
    {
        float accum0 = 0;
        float accum1 = 0;
        float accum2 = 0;
        float accum3 = 0;
        int c = count / 4;

        for (int i = 0; i < c; i++)
        {
            accum0 += shapes[0].Area();
                            accum1 += shapes[1].Area();
                            accum2 += shapes[2].Area();
                            accum3 += shapes[3].Area();
        }
             

             
      

        return accum0+accum1+accum2+accum3;
    }
    
    private float TotalAreaSwitch4(int count, MyShape[] shapes)
    {
        float accum0 = 0;
        float accum1 = 0;
        float accum2 = 0;
        float accum3 = 0;
        int c = count / 4;

        for (int i = 0; i < c; i++)
        {
               accum0 += getArea(shapes[0]);
                        accum1 += getArea(shapes[1]);
                        accum2 += getArea(shapes[2]);
                        accum3 += getArea(shapes[3]);
        }


             
        

        return accum0+accum1+accum2+accum3;
    }

    private float getArea(MyShape myShape)
    {
        //return 0;

switch(myShape.shapeEnum    ){
    case ShapeEnum.Square:return myShape.w * myShape.w;
    case ShapeEnum.Rectangle: return myShape.h * myShape.w;
    case ShapeEnum.Triangle: return 0.5f * myShape.h * myShape.w;
    case ShapeEnum.Circle: return ((float)System.Math.PI) * myShape.w * myShape.w;
    
     
}

         return 0;
    }
}

public abstract class BaseShape
{
    public abstract float Area();
}

public sealed class Square : BaseShape
{
    private float _side;
    public Square(float side) { _side = side; }
    public override float Area() => _side*_side;
}

public sealed class Rectangle : BaseShape
{
    private float _w;
    private float _h;
    public Rectangle(float h, float w)
    {
        _h = h;
        _w = w;
    }
    public override float Area() => _w*_h;
}

public sealed class Triangle : BaseShape
{
    private float _b;
    private float _h;
    public Triangle(float b, float h)
    {
        _b = b;
        _h = h;
    }

    public override float Area() => 0.5f * _b * _h;
}

public sealed class Circle : BaseShape
{
    private float _r;
    public Circle(float r) { _r = r; }
    public override float Area() => ((float)System.Math.PI)*_r*_r;
}

struct MyShape
{
    public ShapeEnum shapeEnum;
    public float w;
    public float h;
}
enum ShapeEnum
{
    Square,
    Rectangle,
    Triangle,
    Circle,
    Count
}
}
